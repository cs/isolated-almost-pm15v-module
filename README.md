# Isolated almost +-15V module
This board provides +-14V from a single supply 5V input, isolating input
and output grounds from each other. The used module can provide 1W in
total, about 30mA per channel.

The idea is to have a standard design which can be stacked on other boards
where bipolar input voltages are needed.

The current revision uses a commercial isolated DC/DC with linear post
regulation to reduce switching noises. This works only partially. Proper
measurements are required, a first check showed switching pulses with high
frequency component (MHz), probably from the MOSFET switching.
