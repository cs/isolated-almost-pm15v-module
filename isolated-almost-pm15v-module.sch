EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L isolated-almost-pm15v-module-rescue:TRV1-0523M-Converter_DCDC U1
U 1 1 60C4CA31
P 4700 3750
F 0 "U1" H 4700 4217 50  0000 C CNN
F 1 "TRV1-0523M" H 4700 4126 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TRV-1M-xxxx_Dual_THT" H 4700 3750 50  0001 C CNN
F 3 "https://www.tracopower.com/sites/default/files/products/datasheets/trv1m_datasheet.pdf" H 4700 3750 50  0001 C CNN
	1    4700 3750
	1    0    0    -1  
$EndComp
$Comp
L isolated-almost-pm15v-module-rescue:USB_B_Micro-Connector J1
U 1 1 60C4D116
P 1000 3750
F 0 "J1" H 1057 4217 50  0000 C CNN
F 1 "USB_B_Micro" H 1057 4126 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Wuerth_651005136421_Vertical" H 1150 3700 50  0001 C CNN
F 3 "~" H 1150 3700 50  0001 C CNN
	1    1000 3750
	1    0    0    -1  
$EndComp
$Comp
L isolated-almost-pm15v-module-rescue:LT3042xMSE-Regulator_Linear U2
U 1 1 60C4F427
P 8550 2600
F 0 "U2" H 8550 2967 50  0000 C CNN
F 1 "LT3042xMSE" H 8550 2876 50  0000 C CNN
F 2 "Package_SO:MSOP-10-1EP_3x3mm_P0.5mm_EP1.68x1.88mm_ThermalVias" H 8550 2925 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/lt3042.pdf" H 8550 2600 50  0001 C CNN
	1    8550 2600
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LT3093xMSE U3
U 1 1 60C4FCAC
P 8550 4950
F 0 "U3" H 8550 5325 50  0000 C CNN
F 1 "LT3093xMSE" H 8550 5416 50  0000 C CNN
F 2 "Package_SO:MSOP-12-1EP_3x4mm_P0.65mm_EP1.65x2.85mm_ThermalVias" H 8550 5375 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/lt3093.pdf" H 8550 5050 50  0001 C CNN
	1    8550 4950
	1    0    0    1   
$EndComp
Wire Wire Line
	5100 3750 5300 3750
Wire Wire Line
	8550 3750 8550 3000
Wire Wire Line
	8550 3750 8550 4550
Connection ~ 8550 3750
Wire Wire Line
	5100 3550 5200 3550
Wire Wire Line
	5200 3550 5200 2500
Wire Wire Line
	5200 2500 5600 2500
Wire Wire Line
	8150 5150 8050 5150
Wire Wire Line
	5200 5150 5200 3950
Wire Wire Line
	5200 3950 5100 3950
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 60C52767
P 1300 3000
F 0 "J2" H 1218 3217 50  0000 C CNN
F 1 "Conn_01x02" H 1218 3126 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 1300 3000 50  0001 C CNN
F 3 "~" H 1300 3000 50  0001 C CNN
	1    1300 3000
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 60C53975
P 10700 3750
F 0 "J3" H 10780 3792 50  0000 L CNN
F 1 "Conn_01x03" H 10780 3701 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 10700 3750 50  0001 C CNN
F 3 "~" H 10700 3750 50  0001 C CNN
	1    10700 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10500 3750 9550 3750
$Comp
L power:GND #PWR01
U 1 1 60C552CB
P 5450 3900
F 0 "#PWR01" H 5450 3650 50  0001 C CNN
F 1 "GND" H 5455 3727 50  0000 C CNN
F 2 "" H 5450 3900 50  0001 C CNN
F 3 "" H 5450 3900 50  0001 C CNN
	1    5450 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3900 5450 3750
Connection ~ 5450 3750
Wire Wire Line
	5450 3750 5600 3750
Wire Wire Line
	10400 2500 10400 3650
Wire Wire Line
	10400 3650 10500 3650
Wire Wire Line
	10500 3850 10400 3850
Wire Wire Line
	10400 3850 10400 5150
Wire Wire Line
	10400 5150 9550 5150
Wire Wire Line
	8050 4750 8150 4750
Wire Wire Line
	8050 2800 8150 2800
$Comp
L Device:C C9
U 1 1 60C59C0C
P 7650 4350
F 0 "C9" H 7765 4396 50  0000 L CNN
F 1 "1u" H 7765 4305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7688 4200 50  0001 C CNN
F 3 "~" H 7650 4350 50  0001 C CNN
	1    7650 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 60C5A2A3
P 7650 3200
F 0 "C8" H 7765 3246 50  0000 L CNN
F 1 "1u" H 7765 3155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7688 3050 50  0001 C CNN
F 3 "~" H 7650 3200 50  0001 C CNN
	1    7650 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 4500 7650 4750
Wire Wire Line
	7650 4750 8050 4750
Connection ~ 8050 4750
Wire Wire Line
	7650 3050 7650 2800
Wire Wire Line
	7650 2800 8050 2800
Connection ~ 8050 2800
$Comp
L Device:R R5
U 1 1 60C5C5B2
P 9050 4400
F 0 "R5" H 9120 4446 50  0000 L CNN
F 1 "56k" H 9120 4355 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8980 4400 50  0001 C CNN
F 3 "~" H 9050 4400 50  0001 C CNN
	1    9050 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 4550 9050 4850
Wire Wire Line
	9050 4850 8950 4850
Wire Wire Line
	9050 4250 9050 3750
Connection ~ 9050 3750
Wire Wire Line
	9050 3750 8550 3750
$Comp
L Device:R R4
U 1 1 60C5DEDD
P 9050 3250
F 0 "R4" H 9120 3296 50  0000 L CNN
F 1 "3k3" H 9120 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8980 3250 50  0001 C CNN
F 3 "~" H 9050 3250 50  0001 C CNN
	1    9050 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8150 5050 8050 5050
Wire Wire Line
	8050 5050 8050 5150
Connection ~ 8050 5150
Wire Wire Line
	8150 2600 8050 2600
Wire Wire Line
	8050 2600 8050 2500
Connection ~ 8050 2500
Wire Wire Line
	8050 2500 8150 2500
NoConn ~ 8150 4850
Wire Wire Line
	9550 4200 9550 3750
Connection ~ 9550 3750
Wire Wire Line
	9550 4500 9550 5150
Connection ~ 9550 5150
$Comp
L Device:C C2
U 1 1 60C6EA9D
P 2750 3750
F 0 "C2" H 2865 3796 50  0000 L CNN
F 1 "22u 25V" H 2865 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2788 3600 50  0001 C CNN
F 3 "~" H 2750 3750 50  0001 C CNN
	1    2750 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 60C6EE84
P 3300 3750
F 0 "C3" H 3415 3796 50  0000 L CNN
F 1 "22u 25V" H 3415 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3338 3600 50  0001 C CNN
F 3 "~" H 3300 3750 50  0001 C CNN
	1    3300 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 60C6F48E
P 3050 3550
F 0 "L1" V 3240 3550 50  0000 C CNN
F 1 "2u2" V 3149 3550 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3050 3550 50  0001 C CNN
F 3 "~" H 3050 3550 50  0001 C CNN
	1    3050 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 3550 3300 3550
Wire Wire Line
	3300 3600 3300 3550
Connection ~ 3300 3550
Wire Wire Line
	3300 3550 3900 3550
Wire Wire Line
	2900 3550 2750 3550
Wire Wire Line
	2100 3950 2750 3950
Wire Wire Line
	3300 3900 3300 3950
Connection ~ 3300 3950
Wire Wire Line
	3300 3950 3900 3950
Text Notes 2850 3200 0    50   ~ 0
suggested EMI filter for class B with 5V input\nfor 12, 15 or 24V use 10uF and 10uH\nand replace TVS diode with 30V version
Wire Wire Line
	4550 4450 4200 4450
Wire Wire Line
	4200 4450 4200 3950
Connection ~ 4200 3950
Wire Wire Line
	4200 3950 4300 3950
Wire Wire Line
	5300 4450 5300 3750
Connection ~ 5300 3750
Wire Wire Line
	5300 3750 5450 3750
$Comp
L Device:D_Schottky D1
U 1 1 60C7CB0C
P 3900 3750
F 0 "D1" V 3854 3830 50  0000 L CNN
F 1 "SMBJ5.0A" V 3945 3830 50  0000 L CNN
F 2 "Diode_SMD:D_SMB" H 3900 3750 50  0001 C CNN
F 3 "~" H 3900 3750 50  0001 C CNN
	1    3900 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	3900 3600 3900 3550
Connection ~ 3900 3550
Wire Wire Line
	3900 3550 4300 3550
Wire Wire Line
	3900 3900 3900 3950
Connection ~ 3900 3950
Wire Wire Line
	3900 3950 4200 3950
$Comp
L Device:C C6
U 1 1 60C87E94
P 5600 3250
F 0 "C6" H 5715 3296 50  0000 L CNN
F 1 "22u 25V" H 5715 3205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5638 3100 50  0001 C CNN
F 3 "~" H 5600 3250 50  0001 C CNN
	1    5600 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3400 5600 3750
Connection ~ 5600 3750
Wire Wire Line
	5600 3100 5600 2500
Connection ~ 5600 2500
$Comp
L Device:C C7
U 1 1 60C8D324
P 5600 4250
F 0 "C7" H 5715 4296 50  0000 L CNN
F 1 "22u 25V" H 5715 4205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 5638 4100 50  0001 C CNN
F 3 "~" H 5600 4250 50  0001 C CNN
	1    5600 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4100 5600 3750
Wire Wire Line
	5600 4400 5600 5150
Connection ~ 5600 5150
Wire Wire Line
	5600 5150 5200 5150
NoConn ~ 1300 3750
NoConn ~ 1300 3850
NoConn ~ 1300 3950
$Comp
L Device:R R1
U 1 1 60C9CA9B
P 900 4600
F 0 "R1" H 970 4646 50  0000 L CNN
F 1 "100k" H 970 4555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 830 4600 50  0001 C CNN
F 3 "~" H 900 4600 50  0001 C CNN
	1    900  4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 60C9D451
P 1300 4600
F 0 "C1" H 1415 4646 50  0000 L CNN
F 1 "100p" H 1415 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1338 4450 50  0001 C CNN
F 3 "~" H 1300 4600 50  0001 C CNN
	1    1300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 4450 1300 4350
Wire Wire Line
	1300 4350 900  4350
Wire Wire Line
	900  4350 900  4150
Wire Wire Line
	900  4350 900  4450
Connection ~ 900  4350
Wire Wire Line
	1000 4150 1000 4250
Wire Wire Line
	2100 4250 2100 3950
Connection ~ 2100 3950
Wire Wire Line
	2100 4250 2100 4850
Wire Wire Line
	2100 4850 1300 4850
Wire Wire Line
	1300 4850 1300 4750
Wire Wire Line
	1300 4850 900  4850
Wire Wire Line
	900  4850 900  4750
Connection ~ 1300 4850
Wire Wire Line
	1300 3550 2100 3550
Wire Wire Line
	8950 2800 9050 2800
Wire Wire Line
	9050 2800 9050 3100
Wire Wire Line
	9050 3400 9050 3750
$Comp
L Device:R R2
U 1 1 60C58A9E
P 8050 3200
F 0 "R2" H 8120 3246 50  0000 L CNN
F 1 "TNPW0603140K" H 8120 3155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7980 3200 50  0001 C CNN
F 3 "~" H 8050 3200 50  0001 C CNN
	1    8050 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 60D0327D
P 8050 4350
F 0 "R3" H 8120 4396 50  0000 L CNN
F 1 "TNPW0603140K" H 8120 4305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7980 4350 50  0001 C CNN
F 3 "~" H 8050 4350 50  0001 C CNN
	1    8050 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 4500 8050 4750
Wire Wire Line
	8050 2800 8050 3050
Text Notes 8850 4250 0    50   ~ 0
limit to 34mA
Text Notes 8850 3150 0    50   ~ 0
limit to 37mA
Text Label 10400 2500 0    50   ~ 0
+14V
Text Label 10400 5150 0    50   ~ 0
-14V
Wire Wire Line
	5600 5150 8050 5150
Wire Wire Line
	5600 2500 8050 2500
Wire Wire Line
	9050 3750 9550 3750
Wire Wire Line
	8950 4950 9150 4950
Wire Wire Line
	9150 4950 9150 5500
Wire Wire Line
	9150 5500 8050 5500
Wire Wire Line
	8050 5500 8050 5150
Wire Wire Line
	8950 2700 9150 2700
Wire Wire Line
	9150 2700 9150 2150
Wire Wire Line
	9150 2150 8050 2150
Wire Wire Line
	8050 2150 8050 2500
NoConn ~ 8150 2700
NoConn ~ 8150 4950
Text Label 2100 4250 0    50   ~ 0
GNDIN
Text Label 1650 3550 0    50   ~ 0
5V
Text Notes 9600 2950 0    50   ~ 0
ESR < 30mOhm\nESL < 1.5nH\nat least 4u7@14V\nsee layout notes \nin datasheets
$Comp
L Mechanical:MountingHole H1
U 1 1 60D51FD5
P 2100 5400
F 0 "H1" H 2200 5446 50  0000 L CNN
F 1 "MountingHole" H 2200 5355 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_DIN965" H 2100 5400 50  0001 C CNN
F 3 "~" H 2100 5400 50  0001 C CNN
	1    2100 5400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 60D5212B
P 2100 5600
F 0 "H2" H 2200 5646 50  0000 L CNN
F 1 "MountingHole" H 2200 5555 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_DIN965" H 2100 5600 50  0001 C CNN
F 3 "~" H 2100 5600 50  0001 C CNN
	1    2100 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 60C76ADF
P 4700 4450
F 0 "C4" V 4448 4450 50  0000 C CNN
F 1 "22pF Y1" V 4539 4450 50  0000 C CNN
F 2 "footprints:C_Disc_D6.0mm_W5.0mm_P10.00mm" H 4738 4300 50  0001 C CNN
F 3 "~" H 4700 4450 50  0001 C CNN
	1    4700 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 4450 5300 4450
Text Notes 4150 4700 0    50   ~ 0
DE11XRA220KA4BQ01F
Wire Wire Line
	1500 2900 2100 2900
Wire Wire Line
	2100 2900 2100 3550
Wire Wire Line
	2100 3950 1850 3950
Wire Wire Line
	1500 3000 1850 3000
Wire Wire Line
	1850 3000 1850 3950
$Comp
L Mechanical:MountingHole H3
U 1 1 60B01286
P 2100 5800
F 0 "H3" H 2200 5846 50  0000 L CNN
F 1 "MountingHole" H 2200 5755 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5_DIN965" H 2100 5800 50  0001 C CNN
F 3 "~" H 2100 5800 50  0001 C CNN
	1    2100 5800
	1    0    0    -1  
$EndComp
$Comp
L isolated-almost-pm15v-module:C_SplitPads C5
U 1 1 60B0D531
P 9550 3100
F 0 "C5" H 9664 3146 50  0000 L CNN
F 1 "10u" H 9664 3055 50  0000 L CNN
F 2 "footprints:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder_SplitPads" H 9588 2950 50  0001 C CNN
F 3 "~" H 9550 3100 50  0001 C CNN
	1    9550 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3250 9550 3750
Wire Wire Line
	9550 2950 9550 2500
Connection ~ 9550 2500
Wire Wire Line
	9550 2500 10400 2500
Wire Wire Line
	8950 2500 9550 2500
Wire Wire Line
	9450 2600 9450 2950
Wire Wire Line
	8950 2600 9450 2600
Wire Wire Line
	7650 3350 7650 3500
Wire Wire Line
	7650 3500 8050 3500
Wire Wire Line
	9450 3500 9450 3250
Wire Wire Line
	8050 3350 8050 3500
Connection ~ 8050 3500
Wire Wire Line
	8050 3500 9450 3500
$Comp
L isolated-almost-pm15v-module:C_SplitPads C10
U 1 1 60B311C0
P 9450 4350
F 0 "C10" H 9306 4396 50  0000 R CNN
F 1 "10u" H 9306 4305 50  0000 R CNN
F 2 "footprints:C_1210_3225Metric_Pad1.42x2.65mm_HandSolder_SplitPads" H 9488 4200 50  0001 C CNN
F 3 "~" H 9450 4350 50  0001 C CNN
	1    9450 4350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8950 5150 9550 5150
Wire Wire Line
	9450 5050 9450 4500
Wire Wire Line
	8950 5050 9450 5050
Wire Wire Line
	5600 3750 8550 3750
Wire Wire Line
	7650 4200 7650 4050
Wire Wire Line
	7650 4050 8050 4050
Wire Wire Line
	9450 4050 9450 4200
Wire Wire Line
	8050 4200 8050 4050
Connection ~ 8050 4050
Wire Wire Line
	8050 4050 9450 4050
Text Notes 5850 3050 0    50   ~ 0
keep some distance in layout between\noutput caps and linear regulators
$Comp
L Device:Polyfuse F1
U 1 1 60D5D1EF
P 2400 3550
F 0 "F1" V 2175 3550 50  0000 C CNN
F 1 "0.5A T" V 2266 3550 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2450 3350 50  0001 L CNN
F 3 "~" H 2400 3550 50  0001 C CNN
	1    2400 3550
	0    1    1    0   
$EndComp
Connection ~ 2100 4250
Wire Wire Line
	1000 4250 2100 4250
Wire Wire Line
	2750 3600 2750 3550
Wire Wire Line
	2750 3900 2750 3950
Connection ~ 2750 3950
Wire Wire Line
	2750 3950 3300 3950
Wire Wire Line
	2550 3550 2750 3550
Connection ~ 2750 3550
Wire Wire Line
	2250 3550 2100 3550
Connection ~ 2100 3550
$EndSCHEMATC
